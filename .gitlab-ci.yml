# SPDX-FileCopyrightText: 2023 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

include:
  - project: 'eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates'
    file: 'pipeline.gitlab-ci.yml'

default:
  tags:
    - origin:eclipse

variables:
  CI_REGISTRY: docker.io
  CI_REGISTRY_IMAGE: docker.io/eclipsecbi
  SCAN_KUBERNETES_MANIFESTS: "true" # https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/application_security/sast/index.md#enabling-kubesec-analyzer
  SECRET_DETECTION_EXCLUDED_PATHS: "config,matrix-*.yaml"
  SAST_EXCLUDED_PATHS: "docker-compose.yaml, docker-compose-federated.yaml"

dco:
  allow_failure: true
  
build-tanka:
  image: grafana/tanka
  stage: build
  parallel:
    matrix:
      - SYNAPSE_ENV: 
          # - "dev"
          # - "staging"
          - "prod"
  script:
    - mkdir -p ./environments/chat-matrix/$SYNAPSE_ENV/.secrets/
    - |
      cat << EOF > ./environments/chat-matrix/$SYNAPSE_ENV/.secrets/secrets.jsonnet
      {}
      EOF
       
    - jb install && tk show --dangerous-allow-redirect "environments/chat-matrix/$SYNAPSE_ENV" > ./matrix-$SYNAPSE_ENV.yaml
  artifacts:
    paths:
      - "./matrix-*.yaml"
    when: on_success
    expire_in: 1 week

.buildkit-rules:
  rules: &buildkit-rules
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: always
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - "$BUILD_CONTEXT_CHANGE/**/*"
        - .gitlab-ci.yml
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - $BUILD_CONTEXT_CHANGE/**/*
        - .gitlab-ci.yml
    - if: '$CI_COMMIT_TAG'
      changes:
        - $BUILD_CONTEXT_CHANGE/**/*
        - .gitlab-ci.yml

buildkit:
  variables:
    BUILD_CONTEXT: docker
    BUILD_CONTEXT_CHANGE: docker
  rules:
    - when: never

container_scanning:
  rules:
    - when: never

# BUILD

build-synapse:
  extends: buildkit
  variables:
    CONTAINER_NAME: synapse
    DOCKERFILE_NAME: Dockerfile.synapse
  rules: *buildkit-rules

build-admin:
  extends: buildkit
  variables:
    CONTAINER_NAME: synapse-admin
    DOCKERFILE_NAME: Dockerfile.admin
  rules: *buildkit-rules

build-compress-state:
  extends: buildkit
  tags:
    - origin:eclipse
    - ctx:medium 
  variables:
    BUILDKIT_ADDRESS: "tcp://buildkitd-fast.foundation-internal-infra-buildkitd:1234"
    CONTAINER_NAME: synapse-compress-state
    DOCKERFILE_NAME: Dockerfile.compress-state
  rules: *buildkit-rules

# CONTAINER_SCANNING

container_scanning-synapse:
  extends: container_scanning
  needs:
    - build-synapse
  variables:
    CONTAINER_NAME: synapse
  rules: *buildkit-rules

container_scanning-admin:
  extends: container_scanning
  needs:
    - build-admin
  variables:
    CONTAINER_NAME: synapse-admin
    IMAGE_TAG: prod
  rules: *buildkit-rules

container_scanning-media-repo:
  extends: container_scanning
  variables:
    CI_REGISTRY_IMAGE: docker.io
    CONTAINER_NAME: turt2live/matrix-media-repo
    IMAGE_TAG: v1.3.1
  rules: *buildkit-rules

container_scanning-compress-state:
  extends: container_scanning
  needs:
    - build-compress-state
  variables:
    CONTAINER_NAME: synapse-compress-state
  rules: *buildkit-rules

# HADOLINT

hadolint:
  variables:
    DOCKERFILE_CONTEXT: docker
    DOCKERFILE_CONTEXT_CHANGE: docker/
    BUILD_CONTEXT_CHANGE: docker
  rules:
    - when: never

hadolint-synapse:
  extends: hadolint
  variables:
    DOCKERFILE_NAME: Dockerfile.synapse
  rules: *buildkit-rules

hadolint-admin:
  extends: hadolint
  variables:
    DOCKERFILE_NAME: Dockerfile.admin
  rules: *buildkit-rules

hadolint-compress-state:
  extends: hadolint
  variables:
    DOCKERFILE_NAME: Dockerfile.compress-state
  rules: *buildkit-rules
  allow_failure: true

# OTHER ANALYSIS

iac-sast:
  needs: [build-tanka]

sast:
  needs: [build-tanka]

secret_detection:
  needs: [build-tanka]
