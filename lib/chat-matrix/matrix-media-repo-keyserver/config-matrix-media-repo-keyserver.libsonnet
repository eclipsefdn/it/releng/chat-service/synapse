
local util = import '../util.libsonnet';

{
  _config+:: {    
    local config = self,
    matrixMediaRepoKeyServer+: {
      local deployment = self,
      local volume = self.volume,
      active:true,
      name: 'matrix-media-repo-keyserver',
      shortName: 'mmrks',
      host: util.getDomain(config.matrixMediaRepoKeyServerDomain, config.environment),
      path: '/',
      version:"latest",
      image: 'docker.io/eclipsecbi/synapse:' + self.version,
      replicas: 1,
      imagePullPolicy: 'Always',
      containerPort: 8008,
      containerPortService: 80,
      env+: {
        SYNAPSE_SERVER_NAME: deployment.host,
        SYNAPSE_CONFIG_PATH: volume.homeserver.path + '/homeserver.yaml',
        SYNAPSE_CACHE_FACTOR: '2.0',
        SYNAPSE_LOG_LEVEL: 'INFO',
      },
      resources+: {
        cpuRequest: '1000m',
        cpuLimit: '2000m',
        memoryRequest: '250Mi',
        memoryLimit: '1000Mi',
      },
      probe+: {
        local probePath = '/_matrix/client/versions',
        readiness+: {
          path: probePath,
          initialDelaySeconds: 10,
          periodSeconds: 3,
          failureThreshold: 3,
          timeoutSeconds: 10,
        },
        liveness+: {
          path: probePath,
          initialDelaySeconds: 120,
          periodSeconds: 10,
          failureThreshold: 3,
          timeoutSeconds: 10,
        },
      },
      volume+: {
        local volume = self,
        local rootMountPath = '/synapse',
        data+: {
          path: rootMountPath + '/data',
        },
        db+: {
          path: rootMountPath + '/db',
        },
        keys+: {
          path: rootMountPath + '/keys',
        },
        homeserver+: {
          path: rootMountPath + '/config',
        },
        log+: {
          path: rootMountPath + '/log',
        },
      },
    },
  },
}
