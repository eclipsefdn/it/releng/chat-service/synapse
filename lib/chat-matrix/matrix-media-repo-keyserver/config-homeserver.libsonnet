local util = import '../util.libsonnet';
{

  local secret = $._secret,
  _secret+:: {
    "matrix-media-repo-keyserver"+: {
      form_secret: 'XXXXXXXXXXXXXXXX',
      macaroon_secret_key: 'XXXXXXXXXXXXXXXX',
      registration_shared_secret: 'XXXXXXXXXXXXXXXX',
      "merged.signing.key": 'XXXXXXXXXXXXXXXX',
    },
  },
  _config+:: {
    local config = self,
    local clientDomain = util.getDomain(config.chatDomain, config.environment),
    local mmrMxDomain = util.getDomain(config.matrixMediaRepoDomain, config.environment),

    matrixMediaRepoKeyServer+: {
      homeserver+: {
        local homeserver = self,
        server_name: mmrMxDomain,
        pid_file: '/synapse/pid/homeserver.pid',
        allow_public_rooms_over_federation: true,
        listeners: [
          {
            port: 8008,
            tls: false,
            type: 'http',
            x_forwarded: true,
            resources: [
              {
                names: [
                  'client',
                  'federation'
                ],
                compress: false,
              },
            ],
          }
        ],
        database: {
          name: 'sqlite3',
          args: {
            database: '/synapse/db/homeserver.db',
          },
        },
        log_config: '/synapse/log/' + mmrMxDomain + '.log.config.yaml',
        enable_authenticated_media: true,
        enable_media_repo: true,
        media_store_path: '/synapse/data/media_store',
        registration_shared_secret: secret["matrix-media-repo-keyserver"].registration_shared_secret,
        report_stats: false,
        macaroon_secret_key: secret["matrix-media-repo-keyserver"].macaroon_secret_key,
        form_secret: secret["matrix-media-repo-keyserver"].form_secret,
        signing_key_path: '/synapse/keys/' + mmrMxDomain + '.signing.key',
        // old_signing_keys: null,
        trusted_key_servers: [
          {
            server_name: 'matrix.org'
          },
        ],
        email: {
          smtp_host: 'mail.eclipse.org',
          enable_tls: false,
          notif_from: '%(app)s <no-reply@eclipse.org>',
          app_name: 'Chat Service Key Server at Eclipse',
          enable_notifs: true,
          notif_for_new_users: true,
          client_base_url: 'https://' + clientDomain,
          invite_client_location: 'https://' + clientDomain,
        },
      },
    },
  },
}
