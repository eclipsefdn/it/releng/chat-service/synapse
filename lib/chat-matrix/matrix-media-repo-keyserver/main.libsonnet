
local kausal = import 'ksonnet-util/kausal.libsonnet';

local util = import '../util.libsonnet';

(import '../config.libsonnet') +
(import './config-matrix-media-repo-keyserver.libsonnet') +
(import './config-logconfig.libsonnet') +
(import './config-homeserver.libsonnet') +
{  
  local this = self,
  local k = kausal { _config+:: this._config },
  
  local container = k.core.v1.container,
  local containerPort = k.core.v1.containerPort,
  local deployment = k.apps.v1.deployment,
  local configMap = k.core.v1.configMap,
  local secret = k.core.v1.secret,  
  local config = $._config.matrixMediaRepoKeyServer,
  local secrets = $._secret["matrix-media-repo-keyserver"],
  local service = k.core.v1.service,
  local servicePort = k.core.v1.service.mixin.spec.portsType,
  local route = import '../../okd/networking/route.libsonnet',

  local labels = util.withLabels($._config, config.name),
  local namespace = $._config.namespace,

  matrixMediaRepoKeyServer: {

    local logconfigName = config.name + '-log',
    logconfig: util.configMap(logconfigName, namespace, labels, 
      {[util.getDomain($._config.matrixMediaRepoDomain, $._config.environment) + '.log.config.yaml']: 
        std.manifestYamlDoc(config.logconfig, indent_array_in_object=true, quote_keys=false)}
    ),

    local signingName = config.name + '-keys',
    signing: util.secretData(signingName, namespace, labels, 
      {[util.getDomain($._config.matrixMediaRepoDomain, $._config.environment) + '.signing.key']: std.base64(secrets["merged.signing.key"],)}
    ),

    local homeserverName = config.name + '-homeserver',
    homeserver: util.secretStringData(homeserverName, namespace, labels, 
      {'homeserver.yaml': std.manifestYamlDoc(config.homeserver,indent_array_in_object=true, quote_keys=false)}
    ),
    
    container:: util.defaultContainer(config),

    deployment: util.deployment(config, namespace, labels, self.container) +   
      deployment.configVolumeMount(logconfigName, config.volume.log.path) +
      deployment.secretVolumeMount(signingName, config.volume.keys.path, volumeMountMixin={readOnly: true}) +
      deployment.secretVolumeMount(homeserverName, config.volume.homeserver.path, volumeMountMixin={readOnly: true}) +
      deployment.emptyVolumeMount(config.name + '-data', config.volume.data.path)+
      deployment.emptyVolumeMount(config.name + '-db', config.volume.db.path),

    service: util.service(self.deployment, namespace, labels),
    route: util.route(config, namespace, labels)
  },
}