{
  _config+:: {
    matrixMediaRepoKeyServer+: {
      logconfig+: {
        version: 1,
        formatters+: {
          precise+: {
            format: '%(asctime)s - %(levelname)s - %(name)s - %(lineno)d - %(request)s - %(message)s',
          },
        },
        handlers+: {
          console+: {
            class: 'logging.StreamHandler',
            formatter: 'precise',
          }
        },
        root+: {
          level: 'INFO',
          handlers+: [
            'console',
          ],
        },
        loggers+: {
          synapse: {
            level: 'INFO',
          },
        },
      },

    },
  },
}
