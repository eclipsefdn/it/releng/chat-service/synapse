
local kausal = import 'ksonnet-util/kausal.libsonnet';
local util = import '../util.libsonnet';

(import '../synapse/config-homeserver.libsonnet') +
(import '../config.libsonnet') +
(import './config.libsonnet') +
(import './config-synapse-compress-state.libsonnet') +
{  
  local this = self,
  local k = kausal { _config+:: this._config },
  
  local container = k.core.v1.container,
  local secret = k.core.v1.secret,  
  local cronJob = k.batch.v1.cronJob,
  local envFrom = k.core.v1.envFromSource,
  local config = $._config.synapseCompressState,
  local labels = util.withLabels($._config, config.name),
  local namespace = $._config.namespace,

  synapseCompressState:{
    local secretName = config.name + '-token',
    [ if config.active then 'compress-state-secret']: util.secretData(secretName, namespace, labels, config.envFromSecret), 

    containerCompress:: if config.active then util.defaultContainer(config) +
      container.withEnvFrom([
        envFrom.secretRef.withName(secretName)
      ]) +
      container.withCommand(['sh', '-c','synapse_auto_compressor -p "host=$POSTGRES_HOST port=$POSTGRES_PORT user=$POSTGRES_USER password=$POSTGRES_PASSWORD dbname=$POSTGRES_DB" -c 500 -n 100']) ,

    [if config.active then 'compress-state-cronjob']: util.batch(config, namespace, labels, self.containerCompress) +
      cronJob.spec.jobTemplate.spec.template.spec.withRestartPolicy(config.restartPolicy) + 
      cronJob.spec.withSuccessfulJobsHistoryLimit(config.successfulJobsHistoryLimit),
  },  
}