local util = import '../util.libsonnet';

{
  local secret = $._secret,
  _secret+:: {
    "synapseCompressState"+: {
      database_password: secret.synapse.database_password,
    },
  },
  _config+:: {
    local config = self,
    local mxDomain = util.getDomain(config.matrixDomain, config.environment),
    synapseCompressState+: {
      config+: {        
        user: if (config.environment == 'prod') then 'synapse_rw' else 'synapse-' + config.environment + '_rw',
        password: secret.synapseCompressState.database_password,
        database: if (config.environment == 'prod') then 'synapse' else 'synapse-' + config.environment + '',
        host: 'postgres-vm1',
        port: '5432',
      },
    },
  },

}
