local util = import '../util.libsonnet';
{
  _config+:: {    
    local config = self,
    synapseCompressState+: {
      active:true,
      name: 'synapse-compress-state',
      shortName: 'compress',
      image: 'docker.io/eclipsecbi/synapse-compress-state:latest',
      imagePullPolicy: 'Always',
      restartPolicy: 'Never',
      successfulJobsHistoryLimit: 1,
      schedule: '0 0 * * *', #Run once a day at midnight
      containerPort: 8080,
      envFromSecret+: {
        POSTGRES_PASSWORD: std.base64(config.synapseCompressState.config.password),
      },
      env+: {
        POSTGRES_HOST: config.synapseCompressState.config.host,
        POSTGRES_PORT: config.synapseCompressState.config.port,
        POSTGRES_DB: config.synapseCompressState.config.database,
        POSTGRES_USER: config.synapseCompressState.config.user,
      },
      resources+: {
        cpuRequest: '250m',
        cpuLimit: '2000m',
        memoryRequest: '250Mi',
        memoryLimit: '2000Mi',
      },
    },
  },
}
