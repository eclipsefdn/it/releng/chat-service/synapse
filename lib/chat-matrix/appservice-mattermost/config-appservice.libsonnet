local util = import '../util.libsonnet';

{
  local secret = $._secret,
  _secret+:: {
    appserviceMattermost+: {
      asToken: "XXXXXXXXXXXXXXXX",
      hsToken: "YYYYYYYYYYYYYYYY",
    }
  },
  _config+:: {    
    local config = self,
    local mxDomain = util.getDomain(config.matrixDomain, config.environment),
    appserviceMattermost+: {
      appservice+: {
        asToken: secret.appserviceMattermost.asToken,
        hsToken: secret.appserviceMattermost.hsToken,        
        botName: "ef_mattermost_bot",
      },
    }
  },
}
