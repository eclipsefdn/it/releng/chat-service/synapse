local util = import '../util.libsonnet';

{
  _config+:: {
    local config = self,
    local mxDomain = util.getDomain(config.matrixDomain, config.environment),

    appserviceMattermost+: {
      registration+: {
        id: 'appservice_Mattermost',
        as_token: config.appserviceMattermost.appservice.asToken,
        hs_token: config.appserviceMattermost.appservice.hsToken,
        sender_localpart: config.appserviceMattermost.appservice.botName,
        namespaces: {
          aliases: [
            {
              exclusive: false,
              regex: '#.*:' + mxDomain,
            },
          ],
          rooms: [],
          users: [
            {
              exclusive: false,
              regex: '@.*:' + mxDomain,
            },
          ],
        },
        rate_limited: false,
        url: null,
      },
    },
  },
}
