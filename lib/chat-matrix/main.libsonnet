(import 'common.libsonnet') + 
(import './matrix-media-repo/main.libsonnet') +
(import './matrix-media-repo-keyserver/main.libsonnet') +
(import './synapse-admin/main.libsonnet') +
(import './synapse/main.libsonnet') +
(import './synapse-nginx/main.libsonnet') +
(import './appservice-slack/main.libsonnet') +
(import './appservice-policies/main.libsonnet') +
(import './pantalaimon/main.libsonnet') +
(import './bot-mjolnir/main.libsonnet')+
(import './synapse-compress-state/main.libsonnet')