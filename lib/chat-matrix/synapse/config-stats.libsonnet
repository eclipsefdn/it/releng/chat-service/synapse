local util = import '../util.libsonnet';
{
  _config+:: {    
    local config = self,
    stats+: {
      active:true,
      name: 'synapse-stats',
      shortName: 'synstats',
      image: 'eclipsecbi/synapse-stats-prometheus:latest',
      imagePullPolicy: 'Always',
      restartPolicy: 'Never',
      containerPort: 3000,
      env+: {
      },
      resources+: {
        cpuRequest: '100m',
        cpuLimit: '100m',
        memoryRequest: '100Mi',
        memoryLimit: '100Mi',
      },
    },
  },
}
