#!/bin/bash

# SPDX-FileCopyrightText: 2022 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# Bash strict-mode
set -o errexit
set -o nounset
#set -o pipefail

environment="${1:-}"

WORKSPACE="$(pwd)/signing"
WORKDIR="${WORKSPACE}/${environment}"
WORKDIR_SYNAPSE="${WORKDIR}/synapse"
WORKDIR_MRR="${WORKDIR}/mmr"

[[ -z "$environment" ]] && echo "Please provide environment as first argument" && exit 1
environmentLink="-$environment"
[[ "$environment" == "prod" ]] && environmentLink=""

PASS_SYNAPSE_PATH="IT/services/chat-service/synapse/${environment}"
PASS_MMR_PATH="IT/services/chat-service/matrix-media-repo/${environment}"
PASS_MMR_KEYSERVER_PATH="IT/services/chat-service/matrix-media-repo-keyserver/${environment}"

matrixDomain=""
mmrDomain=""
if [[ "$environment" == "prod" ]] || [[ "$environment" == "staging" ]]; then
  matrixDomain="matrix${environmentLink}.eclipse.org"
  mmrDomain="matrix-media-repo${environmentLink}.eclipsecontent.org"
else
  matrixDomain="${environment}.matrix.eclipsecontent.org"
  mmrDomain="${environment}.matrix-media-repo.eclipsecontent.org"
fi

echo "################## Environment: ${environment} ##################"

if [[ ! -d "${WORKSPACE}" ]]; then
    mkdir -p "${WORKSPACE}"
fi

if [[ -d "${WORKDIR}" ]]; then
    sudo rm -Rf "${WORKDIR}"
    mkdir -p "${WORKDIR}"
fi

echo "################## Downloading tools ##################"

if [[ ! -f "${WORKSPACE}/generate_signing_key" ]]; then
    curl -L -o "${WORKSPACE}/generate_signing_key" https://github.com/t2bot/matrix-media-repo/releases/download/v1.3.7/generate_signing_key-linux-x64
    chmod +x "${WORKSPACE}/generate_signing_key"
fi
if [[ ! -f "${WORKSPACE}/combine_signing_keys" ]]; then
    curl -L -o "${WORKSPACE}/combine_signing_keys" https://github.com/t2bot/matrix-media-repo/releases/download/v1.3.7/combine_signing_keys-linux-x64
    chmod +x "${WORKSPACE}/combine_signing_keys"
fi

echo "################## Generating signing keys for domain: ${matrixDomain} ##################"

mkdir -p "${WORKDIR_SYNAPSE}"

if pass "${PASS_SYNAPSE_PATH}/signing.key" &>/dev/null; then
    echo "Key ${matrixDomain}.signing.key already exists in pass"
    pass "${PASS_SYNAPSE_PATH}/signing.key" > "${WORKDIR_SYNAPSE}/${matrixDomain}.signing.key"
else
    docker run -it  -e SYNAPSE_SERVER_NAME="${matrixDomain}" -e SYNAPSE_REPORT_STATS=no -v "${WORKDIR_SYNAPSE}:/data" docker.io/eclipsecbi/synapse:latest generate
    sudo chmod -Rf 777 "${WORKDIR_SYNAPSE}"
    pass insert -m "${PASS_SYNAPSE_PATH}/signing.key" < "${WORKDIR_SYNAPSE}/${matrixDomain}.signing.key"
fi


"${WORKSPACE}/generate_signing_key" -output "${WORKDIR_SYNAPSE}/${matrixDomain}.mmr.signing.key"
"${WORKSPACE}/combine_signing_keys" -format synapse -output "${WORKDIR_SYNAPSE}/${matrixDomain}.merged.signing.key" "${WORKDIR_SYNAPSE}/${matrixDomain}.signing.key" "${WORKDIR_SYNAPSE}/${matrixDomain}.mmr.signing.key"

pass insert -m "${PASS_SYNAPSE_PATH}/merged.signing.key" < "${WORKDIR_SYNAPSE}/${matrixDomain}.merged.signing.key"
# mmr private key for matrix.eclipse.org
pass insert -m "${PASS_MMR_PATH}/synapse.mmr.signing.key" < "${WORKDIR_SYNAPSE}/${matrixDomain}.mmr.signing.key"

echo "################## Generating signing keys for domain: ${mmrDomain} ##################"

mkdir -p "${WORKDIR_MRR}"

if pass "${PASS_MMR_PATH}/signing.key" &>/dev/null; then
    echo "Key ${mmrDomain}.signing.key already exists in pass"
    pass "${PASS_MMR_KEYSERVER_PATH}/signing.key" > "${WORKDIR_MRR}/${mmrDomain}.signing.key"
else
    docker run -it  -e SYNAPSE_SERVER_NAME="${mmrDomain}" -e SYNAPSE_REPORT_STATS=no -v "${WORKDIR_MRR}:/data" docker.io/eclipsecbi/synapse:latest generate
    sudo chmod -Rf 777 "${WORKDIR_MRR}"
    pass insert -m "${PASS_MMR_KEYSERVER_PATH}/signing.key" < "${WORKDIR_MRR}/${mmrDomain}.signing.key"
fi


"${WORKSPACE}/generate_signing_key" -output "${WORKDIR_MRR}/${mmrDomain}.mmr.signing.key"
"${WORKSPACE}/combine_signing_keys" -format synapse -output "${WORKDIR_MRR}/${mmrDomain}.merged.signing.key" "${WORKDIR_MRR}/${mmrDomain}.signing.key" "${WORKDIR_MRR}/${mmrDomain}.mmr.signing.key"

pass insert -m "${PASS_MMR_KEYSERVER_PATH}/merged.signing.key" < "${WORKDIR_MRR}/${mmrDomain}.merged.signing.key"
pass insert -m "${PASS_MMR_PATH}/mmr.signing.key" < "${WORKDIR_MRR}/${mmrDomain}.mmr.signing.key"

echo "WARN: Don't forget to push the changes to pass, about 'macaroon_secret_key', 'form_secret', 'registration_shared_secret' to ${PASS_MMR_KEYSERVER_PATH}"