
# sources https://gist.github.com/cmuller/518ae8c49c76fb40457ec3065c048b5f
# doc: https://matrix-org.github.io/synapse/latest/modules/third_party_rules_callbacks.html

from synapse.modules.synapse_user_control.module import UserControlModule

__all__ = ["UserControlModule"]
