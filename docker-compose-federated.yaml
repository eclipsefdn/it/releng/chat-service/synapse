
# SPDX-FileCopyrightText: 2022 eclipse foundation
# SPDX-License-Identifier: EPL-2.0

# source: https://github.com/ArcticFoxes-net/Synapse-Docker-Compose/blob/main/docker-compose.yml

version: '3'
services:

  caddy-matrix-federated:
    image: caddy:2-alpine
    container_name: caddy-matrix-federated
    restart: unless-stopped
    ports:
      - "443:443"
    volumes:
      - ./config/federated/caddy:/etc/caddy
    networks:
      matrix:
        aliases:
          - matrix-local.eclipse.org
          - matrix.to
          - matrix-federated.eclipse.org
  
  synapse-local:
    # image: docker.io/eclipsecbi/synapse:latest
    build: 
      context: ./docker
      dockerfile: ./Dockerfile.synapse 
    container_name: synapse-local
    restart: unless-stopped
    environment:
      - SYNAPSE_CONFIG_PATH=/synapse/config/homeserver.yaml
      - SYNAPSE_SERVER_NAME=matrix-local.eclipse.org
      - SYNAPSE_LOG_LEVEL=info
    ports:
      - "8008:8008"
    depends_on:
      - postgres-local
    networks:
      matrix:
      postgres-local:
    volumes:
      - ./config/local/matrix/config:/synapse/config
      - ./config/local/matrix/data:/synapse/data
      - ./config/local/matrix/keys:/synapse/keys
      - ./config/local/matrix/log:/synapse/log
    healthcheck:
      test: curl --fail http://localhost:8008/_matrix/client/versions || exit 1
      interval: 15s
      retries: 5

  synapse-federated:
    # image: docker.io/eclipsecbi/synapse:latest
    build: 
      context: ./docker
      dockerfile: ./Dockerfile.synapse 
    container_name: synapse-federated
    restart: unless-stopped
    environment:
      - SYNAPSE_CONFIG_PATH=/synapse/config/homeserver.yaml
      - SYNAPSE_SERVER_NAME=matrix-federated.eclipse.org
      - SYNAPSE_LOG_LEVEL=info
    ports:
      - "8009:8008"
    depends_on:
      - postgres-federated
    networks:
      matrix:
      postgres-federated:
    volumes:
      - ./config/federated/matrix/config:/synapse/config
      - ./config/federated/matrix/data:/synapse/data
      - ./config/federated/matrix/keys:/synapse/keys
      - ./config/federated/matrix/log:/synapse/log
    healthcheck:
      test: curl --fail http://localhost:8008/_matrix/client/versions || exit 1
      interval: 15s
      retries: 5

  postgres-local:
    image: docker.io/postgres:alpine
    container_name: postgres-local
    restart: unless-stopped
    environment:
      - POSTGRES_USER=synapse
      - POSTGRES_PASSWORD=passwd
      - POSTGRES_DB=synapse
      - POSTGRES_INITDB_ARGS=--encoding=UTF-8 --lc-collate=C --lc-ctype=C
    ports:
      - "5432:5432"
    networks:
      postgres-local:
    healthcheck:    
      test: ["CMD-SHELL", "pg_isready -U $$POSTGRES_USER -d $$POSTGRES_DB"]
      interval: 15s
      timeout: 5s
    shm_size: 1g

  postgres-federated:
    image: docker.io/postgres:alpine
    container_name: postgres-federated
    restart: unless-stopped
    environment:
      - POSTGRES_USER=synapse
      - POSTGRES_PASSWORD=passwd
      - POSTGRES_DB=synapse
      - POSTGRES_INITDB_ARGS=--encoding=UTF-8 --lc-collate=C --lc-ctype=C
    ports:
      - "5433:5432"
    networks:
      postgres-federated:
    healthcheck:    
      test: ["CMD-SHELL", "pg_isready -U $$POSTGRES_USER -d $$POSTGRES_DB"]
      interval: 15s
      timeout: 5s
    shm_size: 1g

  matrix-media-repo-local:
    image: docker.io/turt2live/matrix-media-repo:v1.3.1
    container_name: mediarepo-local
    restart: unless-stopped
    ports:
      - "8001:8000"
    volumes:
      - ./config/local/mediarepo:/data
    networks:
      matrix:
      postgres-local:

  matrix-media-repo-federated:
    # image: docker.io/eclipsecbi/matrix-media-repo:latest
    build: 
      context: ./docker
      dockerfile: ./Dockerfile.mediarepo 
      args: 
        - CLONE_BRANCH=master
    container_name: mediarepo-federated
    restart: unless-stopped
    ports:
      - "8002:8000"
    volumes:
      - ./config/federated/mediarepo:/data
    networks:
      matrix:
      postgres-federated:

  synapse-admin:
    build: 
      context: ./docker
      dockerfile: ./Dockerfile.admin 
      args: 
        - REACT_APP_SERVER=https://matrix-local.eclipse.org
    container_name: synapseadmin
    restart: unless-stopped
    ports:
      - "8070:8080"
    networks:
      - matrix

  mjolnir-local:
    image: matrixdotorg/mjolnir:latest
    container_name: mjolnir-local
    restart: unless-stopped
    ports:
      - "8082:8080"
      # - "8083:8081"
    volumes:
      - ./config/local/mjolnir:/data
    environment:
      NODE_ENV: development
      NODE_TLS_REJECT_UNAUTHORIZED: 0
    depends_on:
      - synapse-local
    networks:
      - matrix
    healthcheck:
      test: curl --fail http://localhost:8080/healthz || exit 1
      interval: 15s
      retries: 5

  pantalaimon-local:
    image: matrixdotorg/pantalaimon:latest
    container_name: pantalaimon-local
    restart: unless-stopped
    volumes:
      - ./config/local/pantalaimon:/data
    ports:
      - "8084:8008"
    depends_on:
      - mjolnir-local
    networks:
      matrix:
        aliases:
          - pantalaimon.eclipse.org


networks:
  matrix:
  postgres-local:
  postgres-federated:
